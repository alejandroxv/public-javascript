// ==UserScript==
// @name         EliminarComentarios
// @namespace    http://chollometro.com/
// @version      1.2
// @description  Elimina Comentarios Masivamente
// @author       @axwll
// @match        https://www.chollometro.com/ofertas/*
// @match        https://www.chollometro.com/cupones/*
// @match        https://www.chollometro.com/temas/*
// @downloadURL https://gitlab.com/alejandroxv/public-javascript/-/raw/master/EliminarComentarios.user.js
// ==/UserScript==

(function() {

var checkExist = setInterval(function() {
if ($('.comment-footer').length) {

async function removeComment(commentID) {
    var sCommentUrl = "https://www.chollometro.com/admin-v2/moderation/comments/delete/" + commentID;
    return $.ajax({
        type: 'POST',
        url: sCommentUrl,
        data: JSON.stringify({
            reason: "Mensaje borrado sin razón"
        }),
        headers: {
            "accept": "application/json, text/plain, */*",
            "accept-language": "es-ES,es;q=0.9",
            "content-type": "application/json;charset=UTF-8",
            "x-requested-with": "XMLHttpRequest"
        }
    }).done(function(data) {});
};

var arrArticles = document.querySelectorAll('.commentList-item');
$.each(arrArticles, function(i, value) {

    var eInput = document.createElement("input");
    eInput.type = "checkbox";
    eInput.setAttribute("class", "massiveComments");
    eInput.value = value.id.replace("comment-", "");
    eInput.style.padding = "20px";
    eInput.style.margin = "40px 20px 20px 20px";
    eInput.style.width = "20px";
    eInput.style.height = "20px";

    var couponPost = value.querySelector('.comment-footer');
    $(couponPost).append(eInput);

});

$('#comments').append('<br><button type="button" class="cept-dealBtn btn btn--mode-primary" id="removeMassiveComments">Remove comments</button><br>');

async function removeMassiveComments (){
	var arrCheckbox = document.querySelectorAll('.massiveComments');
	$.each(arrCheckbox, function(i, value) {
		if(value.checked){
			removeComment(value.value)
		}

	});
}

var iContadorComentarios = 0;
$('.massiveComments').change(function(){
	var sIdComment = "#comment-" + this.value + " .commentList-comment"
    if($(this).is(':checked')) {
		$(sIdComment).css("border", "3px solid red");
		iContadorComentarios ++;
    } else {
		$(sIdComment).css("border", "");
		iContadorComentarios --;

    }

	$("#removeMassiveComments").html("Remove comments (" + iContadorComentarios + ")");
});

$('#removeMassiveComments').on('click', function (){
	$('#removeMassiveComments').css({'background': 'silver'});
	removeMassiveComments();
	window.location.reload();
});
    clearInterval(checkExist);
   }
}, 100);

})();

