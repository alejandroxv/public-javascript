// ==UserScript==
// @name         CMExtended
// @namespace    http://chollometro.com/
// @version      2.8.8
// @description  Mod faster
// @author       @axwll
// @match        https://www.chollometro.com/admin-v2/moderation/*
// @match        https://www.chollometro.com/admin/*
// @match        https://www.chollometro.com/ofertas/*
// @match        https://www.chollometro.com/cupones/*
// @match        https://www.chollometro.com/temas/*
// @require      http://code.jquery.com/jquery-3.3.1.min.js
// @downloadURL  https://gitlab.com/alejandroxv/public-javascript/-/raw/master/CMExtended.user.js
// @updateURL    https://gitlab.com/alejandroxv/public-javascript/-/raw/master/CMExtended.user.js
// @grant        GM.xmlHttpRequest
// ==/UserScript==

(function() {

    // FUNCIONES GLOBALES
    function waitForKeyElements(selectorTxt, actionFunction, bWaitOnce, iframeSelector) {
        var controlObj = waitForKeyElements.controlObj || {};
        var controlKey = selectorTxt.replace(/[^\w]/g, "_");

        if (!controlObj[controlKey]) {
            controlObj[controlKey] = {
                actionFunctions: [],
                interval: null
            };
        }

        controlObj[controlKey].actionFunctions.push(actionFunction);
        waitForKeyElements.controlObj = controlObj;

        function checkAndRunFunctions() {
            var targetNodes;

            if (typeof iframeSelector == "undefined")
                targetNodes = $(selectorTxt);
            else
                targetNodes = $(iframeSelector).contents().find(selectorTxt);

            if (targetNodes && targetNodes.length > 0) {
                targetNodes.each(function() {
                    var jThis = $(this);
                    var alreadyFound = jThis.data('alreadyFound') || false;

                    if (!alreadyFound) {
                        controlObj[controlKey].actionFunctions.forEach(function(actionFunc) {
                            var cancelFound = actionFunc(jThis);
                            if (!cancelFound) {
                                jThis.data('alreadyFound', true);
                            }
                        });
                    }
                });
            }

            if (bWaitOnce && targetNodes && targetNodes.length > 0) {
                clearInterval(controlObj[controlKey].interval);
                delete controlObj[controlKey];
            }
        }

        if (!controlObj[controlKey].interval) {
            controlObj[controlKey].interval = setInterval(checkAndRunFunctions, 300);
        }

        checkAndRunFunctions();
    }

    var waitForJQuery = function(callback) {
        var interval = setInterval(function() {
            if (typeof $ !== 'undefined') {
                clearInterval(interval);
                callback();
            }
        }, 50);
    };

    let oldURL = "";
    const currentURL = window.location.href;
    var waitForPageChange = (callback) => {
        const intervalId = setInterval(() => {
            const currentURL = window.location.href;
            if (currentURL !== oldURL) {
                oldURL = currentURL;
                callback();
            }
        }, 100);
        return intervalId;
    };

    // ACEPTAR COMENTARIOS
    function aceptAllComments() {
        var arrPermLink = document.querySelectorAll(".rounded-medium.border-all.elevation-1.mb-3.v-card.v-sheet");
        var commentIds = [];
        $.each(arrPermLink, function(i) {
            var elementId = this.getAttribute('id');
            commentIds.push(elementId);
        });
        var sCommentUrl = "https://www.chollometro.com/admin-v2/moderation/comments/bulk-approve";
        var payload = JSON.stringify({
            commentIds: commentIds
        });
        console.log(sCommentUrl);
        $.ajax({
            type: 'POST',
            url: sCommentUrl,
            headers: {
                "accept": "application/json, text/plain, */*",
                "accept-language": "es-ES,es;q=0.9",
                "content-type": "application/json;charset=UTF-8",
                "x-requested-with": "XMLHttpRequest"
            },
            data: payload
        }).done(function(data) {});
    }

    function addApproveButton() {
        if (window.location.pathname.includes("admin-v2/moderation/comments/new")) {
            if ($('#aceptAllComments').length === 0) {
                $(".layout.justify-center:last-child").prepend('<button id="aceptAllComments" type="button" class="v-btn v-btn--depressed theme--light primary"><div class="v-btn__content"> Aprobar todos <i aria-hidden="true" class="v-icon ml-2 material-icons theme--light">check_circle</i></div></button>').html;
                $('#aceptAllComments').on('click', function() {
                    aceptAllComments();
                });
            }
        }
    }

    // DESCARTAR REPORTES
    function discartAllReports() {
        $.ajax({
            type: 'GET',
            url: 'https://www.chollometro.com/admin-v2/moderation/comments/reported',
            headers: {
                "accept": "application/json, text/plain, */*",
                "accept-language": "es-ES,es;q=0.9",
                "x-requested-with": "XMLHttpRequest"
            },
            success: function(data) {
                $.each(data.state.commentModeration.items, function(index, item) {
                    $.ajax({
                        type: 'POST',
                        url: `https://www.chollometro.com/admin-v2/moderation/comments/resolve-issue-report/${item.id}`,
                        headers: {
                            "accept": "application/json, text/plain, */*",
                            "accept-language": "es,es-ES;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
                            "content-type": "application/json;charset=UTF-8",
                            "priority": "u=1, i"
                        },
                        referrer: "https://www.chollometro.com/admin-v2/moderation/comments/reported",
                        referrerPolicy: "strict-origin-when-cross-origin",
                        data: JSON.stringify({
                            issueId: item.issue.id
                        }),
                        mode: 'cors',
                        credentials: 'include',
                        success: function(response) {
                            console.log(`Reporte resuelto para Comentario ID: ${item.id}, Issue ID: ${item.issue.id}`);
                        },
                        error: function(error) {
                            console.error(`Error al resolver el reporte para Comentario ID: ${item.id}, Issue ID: ${item.issue.id}`, error);
                        }
                    });
                });
            },
            error: function(error) {
                console.error("Error al obtener los reportes:", error);
            }
        });
    }


    function addDiscardButton() {
        if (window.location.pathname.includes("admin-v2/moderation/comments/reported")) {
            if ($('#discartAllReports').length === 0) {
                $(".layout.justify-center:last-child").prepend('<button id="discartAllReports" type="button" class="v-btn v-btn--depressed theme--light primary"><div class="v-btn__content"> Descartar reportes <i aria-hidden="true" class="v-icon ml-2 material-icons theme--light">cancel</i></div></button>').html;
                $('#discartAllReports').on('click', function() {
                    discartAllReports();
                });
            }
        }
    }

    // Función para habilitar y modificar los botones
    function enableButtons() {
        // Selecciona los botones específicos por sus clases
        var holdButton = document.querySelector('button[disabled].border-all.v-btn.v-btn--disabled.v-btn--outline.v-btn--depressed.theme--light[slot="activator"]');
        var approveButton = document.querySelector('button[disabled].v-btn.v-btn--disabled.v-btn--outline.v-btn--depressed.theme--light.grey--text.text--darken-3.border-all.border-grey.cept-thread-moderation-approve-btn');

        // Quita el atributo 'disabled' y la clase 'v-btn--disabled' del botón "Hold"
        if (holdButton) {
            holdButton.removeAttribute('disabled');
            holdButton.classList.remove('v-btn--disabled');
        }

        // Quita el atributo 'disabled' y la clase 'v-btn--disabled' del botón "Approve"
        if (approveButton) {
            approveButton.removeAttribute('disabled');
            approveButton.classList.remove('v-btn--disabled');
        }
    }

    // BANEAR EMAILS
    function addBanButton() {
        if (window.location.pathname.includes("admin/blacklists/emails")) {
            if ($('#addBanButton').length === 0) {
                $("ds-blacklisted-emails-sidebar").prepend(`
                    <div id="bulkBlacklistContainer">
                        <label for="delayInput">Delay between requests (seconds):</label>
                        <input type="number" id="delayInput" value="1" min="1" style="margin-top: 10px; width: 100%;">
                    <input type="text" id="bulkEmailUrl" placeholder="Enter URL here" style="margin-top: 10px; width: 100%;">
                        <button id="addBanButton" style="background-color: red !important;" class="btn btn-block">
                            <i class="icon-plus"></i><span>Add bulk email</span>
                        </button>
                        <div id="progressContainer" style="margin-top: 10px; display: none;">
                            <span id="progressText"></span>
                            <progress id="progressBar" value="0" max="100" style="width: 100%;"></progress>
                        </div>
                    </div>
                `);
                $('#addBanButton').on('click', function() {
                    var inputUrl = $('#bulkEmailUrl').val();
                    if (inputUrl.includes("gist.githubusercontent.com") && inputUrl.includes("raw")) {
                        fetchEmails(inputUrl);
                    } else {
                        alert("Please enter a valid raw gist URL.");
                    }
                });
            }
        }
    }
    // Función para realizar la solicitud y procesar los emails
    function fetchEmails(url) {
        GM.xmlHttpRequest({
            method: "GET",
            url: url,
            onload: function(response) {
                if (response.status === 200) {
                    console.log("Datos recibidos:", response.responseText);
                    processEmails(response.responseText);
                } else {
                    console.error("Error al obtener los datos:", response.statusText);
                }
            }
        });
    }

    // Función para procesar los emails
    function processEmails(data) {
        const emails = data.split('\n').filter(email => email.trim() !== "");
        const totalEmails = emails.length;

        if (totalEmails > 0) {
            $('#progressContainer').show();
            updateProgress(0, totalEmails);
            processEmailSequentially(emails, 0, totalEmails);
        } else {
            alert("No valid emails found in the provided URL.");
        }
    }

    // Función para actualizar la barra de progreso
    function updateProgress(current, total) {
        const progressPercent = Math.round((current / total) * 100);
        $('#progressBar').val(progressPercent);
        $('#progressText').text(`Processing ${current}/${total} emails (${progressPercent}%)`);
    }

    // Función para procesar emails de manera secuencial
    function processEmailSequentially(emails, index, totalEmails) {
        if (index < totalEmails) {
            const modifiedEmail = `*@${emails[index].trim()}`;
            console.log(modifiedEmail);
            banEmail(modifiedEmail).then(() => {
                updateProgress(index + 1, totalEmails);
                const delay = parseInt($('#delayInput').val(), 10) * 1000; // Convertir segundos a milisegundos
                setTimeout(() => {
                    processEmailSequentially(emails, index + 1, totalEmails);
                }, delay);
            }).catch(() => {
                updateProgress(index + 1, totalEmails);
                const delay = parseInt($('#delayInput').val(), 10) * 1000; // Convertir segundos a milisegundos
                setTimeout(() => {
                    processEmailSequentially(emails, index + 1, totalEmails);
                }, delay);
            });
        }
    }


    // Función para banear el email
    function banEmail(modifiedEmail) {
        return fetch("https://www.chollometro.com/admin/blacklists/emails", {
            method: "POST",
            headers: {
                "accept": "application/json, text/plain, */*",
                "accept-language": "es,es-ES;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
                "content-type": "application/json;charset=UTF-8",
                "priority": "u=1, i",
                "sec-ch-ua-mobile": "?0",
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-origin",
                "x-kl-kis-ajax-request": "Ajax_Request",
            },
            referrer: "https://www.chollometro.com/admin/blacklists/emails",
            referrerPolicy: "strict-origin-when-cross-origin",
            body: JSON.stringify({
                type: "banned",
                email: modifiedEmail,
                ban_reason: "temporal"
            }),
            mode: "cors",
            credentials: "include"
        }).then(response => {
            if (response.ok) {
                console.log(`Email ${modifiedEmail} baneado exitosamente.`);
            } else {
                console.error(`Error al banear el email ${modifiedEmail}:`, response.statusText);
            }
        }).catch(error => {
            console.error(`Error al banear el email ${modifiedEmail}:`, error);
        });
    }

    // ELIMINAR HILOS DE COMENTARIOS
    // Consultar Comentarios
    async function QueryComments(threadId, commentId) {
        try {
            const response = await new Promise((resolve, reject) => {
                GM.xmlHttpRequest({
                    method: "POST",
                    url: "https://www.chollometro.com/graphql",
                    headers: {
                        "accept": "application/json, text/plain, */*",
                        "accept-language": "es,es-ES;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
                        "content-type": "application/json",
                        "priority": "u=1, i",
                        "x-pepper-txn": "threads.show.deal",
                        "x-request-type": "application/vnd.pepper.v1+json",
                    },
                    data: JSON.stringify({
                        query: `query comments($filter: CommentFilter!, $limit: Int, $page: Int) { comments(filter: $filter, limit: $limit, page: $page) { items { commentId mainCommentId threadId } pagination { count current last next previous size order orderBy } messages { title message type context identifier priority code iconName } } }`,
                        variables: {
                            filter: {
                                mainCommentId: commentId,
                                threadId: {
                                    eq: threadId
                                },
                                order: {
                                    direction: "Ascending"
                                }
                            },
                            page: 1,
                            limit: 100,
                            repliesPreview: false
                        }
                    }),
                    onload: response => resolve(JSON.parse(response.responseText)),
                    onerror: reject
                });
            });

            const comments = response.data.comments.items;

            console.log('Comments:', comments);

            // Se elimina el comentario principal
            await deleteComment(commentId);
            // Llamar a la función para eliminar cada comentario
            for (const comment of comments) {
                await deleteComment(comment.commentId);
            }
            // Mostrar un alert después de eliminar todos los comentarios
            alert('Todos los comentarios han sido eliminados.');

        } catch (error) {
            console.error('Error:', error);
        }
    }

    // Eliminar Comentarios
    async function deleteComment(commentId) {
        try {
            const response = await new Promise((resolve, reject) => {
                GM.xmlHttpRequest({
                    method: "POST",
                    url: "https://www.chollometro.com/graphql",
                    headers: {
                        "accept": "application/json, text/plain, */*",
                        "accept-language": "es,es-ES;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
                        "content-type": "application/json",
                        "priority": "u=1, i",
                        "x-pepper-txn": "threads.show.deal",
                        "x-request-type": "application/vnd.pepper.v1+json",
                    },
                    data: JSON.stringify({
                        query: `mutation deleteComment($commentId: ID!, $message: String, $reason: String) { deleteComment(input: { id: $commentId, message: $message, reason: $reason }) { comment { commentId status deletedBy { username } } validation { path translations { title body } } messages { title message type context identifier priority code iconName } } }`,
                        variables: {
                            commentId: commentId,
                            reason: null,
                            message: null
                        }
                    }),
                    mode: "cors",
                    credentials: "include",
                    referrerPolicy: "strict-origin-when-cross-origin",
                    onload: response => resolve(JSON.parse(response.responseText)),
                    onerror: reject
                });
            });

            if (response.data.deleteComment) {
                console.log(`Comment ${commentId} deleted successfully.`);
            } else {
                console.error(`Failed to delete comment ${commentId}:`, response.data.messages);
            }
        } catch (error) {
            console.error('Error:', error);
        }
    }


    // Obtener threadId de la pagina actual
    function getThreadId() {
        var $mainElement = $('#main');

        // Obtén el valor del atributo data-t-d
        var dataTData = $mainElement.attr('data-t-d');

        // Verifica si el atributo data-t-d existe y no está vacío
        if (dataTData) {
            try {
                // Parsear el valor del atributo data-t-d como JSON
                var dataJson = JSON.parse(dataTData);

                // Extraer el threadId del JSON
                return dataJson.threadId;
            } catch (error) {
                console.error('Error parsing JSON:', error);
            }
        } else {
            console.error('El atributo data-t-d no existe o está vacío.');
        }
        return null; // En caso de error o si no existe el atributo
    }

    // Mostrar botones que ejecutan las funciones de eliminar hilos de comentarios
    function removeCommentsThread() {
        // Define el HTML del nuevo botón
        var nuevoHTML = '<button class="space--r-2 text--color-red flex boxAlign-center query-comments-button" style="border: none; background: none; cursor: pointer;" data-comment-id="comment-id">' +
            '<svg width="18" height="20" class="icon icon--trash icon-u--1">' +
            '<use xlink:href="/assets/img/ico_1d9cc.svg#trash"></use>' +
            '</svg>' +
            '<span class="text--b size--all-s space--h-1 hide--toW3"> Eliminar hilo </span>' +
            '</button>';

        // Iterar sobre cada <li> que contenga .comment-padding--hasReply
        $('li.commentList-item').each(function() {
            // Encontrar el primer elemento .comment-padding--hasReply dentro de este <li>
            var $primerElementoConReply = $(this).find('.comment-padding--hasReply').first();

            // Si se encontró el primer elemento .comment-padding--hasReply
            if ($primerElementoConReply.length > 0) {
                // Encuentra el botón específico dentro del primer elemento .comment-padding--hasReply
                var $targetButton = $primerElementoConReply.find('.space--mr-1.button.button--shape-circle.button--type-tertiary.button--mode-default.button--size-s.button--square');

                // Si se encontró el botón objetivo, inserta el nuevo botón antes de él
                if ($targetButton.length > 0) {
                    // Crear un objeto jQuery para el nuevo HTML
                    var $nuevoHTML = $(nuevoHTML);

                    // Reemplaza el atributo data-comment-id con el ID del comentario del <li> actual
                    $nuevoHTML.attr('data-comment-id', $(this).attr('data-id'));

                    // Inserta el nuevo botón antes del botón específico
                    $targetButton.before($nuevoHTML);

                    // Añadir evento click al botón para llamar a QueryComments
                    $nuevoHTML.on('click', function() {
                        // Obtener el threadId y el commentId del botón clicado
                        var threadId = getThreadId();
                        var commentId = $(this).attr('data-comment-id');

                        // Llamar a QueryComments con los parámetros adecuados
                        QueryComments(threadId, commentId);
                    });
                }
            }
        });
    }


    // ONLOAD

    window.onload = function() {
        waitForJQuery(function() {
            waitForPageChange(function() {

                // MUESTRA BOTONES PARA COMENTARIOS Y REPORTES
                if (window.location.pathname.match(/\/admin\-v2\/moderation/)) {
                    // Observa cambios en el DOM para aplicar la función cuando los botones sean agregados dinámicamente
                    var observer = new MutationObserver(function(mutations) {
                        mutations.forEach(function(mutation) {
                            enableButtons();
                        });
                    });

                    // Llamadas a waitForKeyElements para cada función de botón
                    waitForKeyElements(".layout.justify-center:last-child", addApproveButton, false);
                    waitForKeyElements(".layout.justify-center:last-child", addDiscardButton, false);

                    observer.observe(document.body, {
                        childList: true,
                        subtree: true
                    });

                    // Llama a la función inicialmente por si los botones ya están presentes
                    enableButtons();
                }

                // MUESTRA BOTON PARA BANEO DE EMAILS
                if (window.location.pathname.match(/\/admin\/blacklists\/emails/)) {
                    // Esperar a que aparezca el elemento ds-blacklisted-emails-sidebar y ejecutar addBanButton
                    waitForKeyElements("ds-blacklisted-emails-sidebar", addBanButton, false);
                }
                // MUESTRA BOTONES PARA ELIMINAR HILOS DE COMENTARIOS
                if (window.location.pathname.match(/-\d{3,10}/) && !window.location.pathname.match(/\/edit/) && !window.location.pathname.match(/\/admin/)) {

                    waitForKeyElements("#thread-comments", removeCommentsThread, false);

                }

            }); // Cierra la función waitForPageChange
        }); // Cierra la función waitForJQuery
    }; // Cierra la función window.onload


})();