// ==UserScript==
// @name         ExpireCoupon
// @version      1.1
// @description  Expira multiples post con un mismo cupón
// @author       @axwll
// @match        https://www.chollometro.com/search?*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

async function expireCoupon() {
 var jsonSeach = JSON.parse('{"' + decodeURI(window.location.search.substring(1)).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
 var coupon = jsonSeach.q;
 var arrArticles = document.querySelectorAll('div > article');
 $.each( arrArticles, function( i, value ) {
	 var couponPost = value.querySelector('.js-voucherCode > div > input');
     if(couponPost != null){
         var className = value.className
         var threadID = value.id.replace("thread_", "");
         if (couponPost.value.includes(coupon) && !className.includes('thread--expired')){
             expirePost(threadID);
         }
     }
 });
};

function getDealJSON(ajaxurl) {
 return $.ajax({
		type: 'GET',
		url: ajaxurl
			}).done(function(oJson) {
			});
};

async function expirePost(threadID) {
	var sApproveURL = "https://www.chollometro.com/admin-v2/moderation/thread/expire/" + threadID;
    $.ajax({
        type: 'POST',
        url: sApproveURL,
        headers: {
            "accept":"application/json, text/plain, */*"
            ,"accept-language":"es-ES,es;q=0.9"
            ,"content-type":"application/json;charset=UTF-8"
            ,"x-requested-with":"XMLHttpRequest"
        }}).done(function(data) {
    });

};

if (window.location.pathname.match(/\/search/)) {
	$('.subNavMenu-list.cept-sort-tabs').append('<br><li style="width: 60%;"><button type="button" class="btn link" id="expireCoupon">Expire Coupon</button></li><br>');
}

$('#expireCoupon').on('click', function (){
	$('#expireCoupon').css({'background': 'silver'});
	expireCoupon();
});


})();