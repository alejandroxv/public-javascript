// ==UserScript==
// @name         ChollometroTools
// @namespace    https://www.chollometro.com/
// @version      1.4.0
// @description  Herramientas para los moderadores
// @author       @axwll
// @match        https://www.chollometro.com/*
// @grant        none
// @downloadURL https://gitlab.com/alejandroxv/public-javascript/-/raw/master/ChollometroTools.user.js
// @updateURL   https://gitlab.com/alejandroxv/public-javascript/-/raw/master/ChollometroTools.user.js
// ==/UserScript==
(function() {
    'use strict';

    // FUNCIONES GENERALES
    var waitForEl = function(selector, callback) {
        if (document.querySelector(selector)) {
            callback();
        } else {
            setTimeout(function() {
                waitForEl(selector, callback);
            }, 100);
        }
    };

    var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    var dataBase = null;

    function startDB(callback) {
        dataBase = indexedDB.open("chollometro_tools", 1);
        var args = Array.prototype.slice.call(arguments, 1); // Captura argumentos adicionales

        dataBase.onupgradeneeded = function(e) {
            var active = dataBase.result;
            var object = active.createObjectStore('thread', {
                keyPath: 'id',
                autoIncrement: true
            });
            object.createIndex('by_thread_id', 'thread_id', {
                unique: true
            });
        };

        dataBase.onsuccess = function(e) {
            callback.apply(null, [dataBase.result].concat(args)); // Pasa la base de datos y otros argumentos
        };

        dataBase.onerror = function(e) {
            console.error('Error loading database');
        };
    }

    function loadAll(db, htmlTemplate, positionSelector) {
        var active = db;
        var data = active.transaction(["thread"], "readonly");
        var object = data.objectStore("thread");

        var elements = [];
        object.openCursor().onsuccess = function(e) {
            var result = e.target.result;
            if (result === null) {
                return;
            }
            elements.push(result.value);
            result.continue();
        };

        data.oncomplete = function() {
            if (htmlTemplate && positionSelector) {
                // Modo de creación completa del HTML
                var optionsHtml = elements.map(function(element) {
                    return '<option value="' + element.thread_id + '">' +
                        element.id + ' -> ' + element.thread_id + ' - ' + element.name +
                        '</option>';
                }).join('');

                var finalHtml = htmlTemplate.replace('<!-- OptionsPlaceholder -->', optionsHtml);
                var positionElement = document.querySelector(positionSelector);
                if (positionElement) {
                    positionElement.innerHTML = finalHtml;
                }
            } else {
                // Modo de solo adición de opciones a un select existente
                var threadSelect = document.getElementById('thread_id_select');
                if (threadSelect) {
                    elements.forEach(function(element) {
                        var option = document.createElement('option');
                        option.value = element.thread_id;
                        option.textContent = element.id + ' -> ' + element.thread_id + ' - ' + element.name;
                        threadSelect.appendChild(option);
                    });
                }
            }
        };
    }


    function addThreadId(db, threadId, threadName) {
        var active = db;
        var data = active.transaction(["thread"], "readwrite");
        var object = data.objectStore("thread");

        var request = object.add({
            thread_id: threadId,
            name: threadName
        });

        request.onsuccess = function(e) {
            alert("Thread added successfully");
        };

        request.onerror = function(e) {
            console.error("Error adding thread", e.target.error.name);
        };
    }

    function deleteObject(db, keyPathId) {
        var active = db;
        var data = active.transaction(["thread"], "readwrite");
        var object = data.objectStore("thread");

        var request = object.delete(keyPathId);

        request.onsuccess = function(e) {
            alert("Thread deleted successfully");
        };

        request.onerror = function(e) {
            console.error("Error deleting thread", e.target.error.name);
        };
    }

    // Eventos y manipulación del DOM...

    window.onload = function(event) {
        // Logica específica para las rutas en la página
        if (window.location.pathname.match(/\/ofertas|temas|cupones/) && !window.location.pathname.match(/\/admin/)) {
            $.each({
                "delete_thread_id": "  -Eliminar ID",
                "add_thread_id": "  +Agregar ID"
            }, function(i, value) {
                var eLabelA = document.createElement("a");
                var eSvg = document.createElement("svg");
                eLabelA.setAttribute("class", "button button--type-text button--mode-secondary space--mr-4");
                eLabelA.setAttribute("style", "color:green;");
                eLabelA.setAttribute("id", i);
                eSvg.setAttribute("width", "21");
                eSvg.setAttribute("height", "21");
                eSvg.setAttribute("class", "icon space--mr-2");
                eLabelA.appendChild(eSvg);
                eLabelA.appendChild(document.createTextNode(value));
                $('.js-options').prepend(eLabelA);

            });

            var sPositionSelect = "#thread-merge > ul > li:nth-child(2) > div";
            var htmlTemplate = '<div style="border: 1px solid #d1d5db;">' +
                '<select style="width: 100%;" id="thread_id_select">' +
                '<option value=""></option>' +
                '<!-- OptionsPlaceholder -->' +
                '</select></div><br>';
            $(document).on("click", 'a[data-popover*="merge"]', function() {
                waitForEl('#thread-merge', function() {
                    startDB(loadAll, htmlTemplate, sPositionSelect);
                })
            });


            $(document).on("change", "#thread_id_select", function() {
                $('#thread-merge-destination_thread_id').val(this.value);
            });

            $('#add_thread_id').on('click', function() {
                var sThreadId = prompt("Write thread id");
                var sNameThread = prompt("Write a name for thread id");

                if (sThreadId != null && sNameThread != null) {
                    startDB(addThreadId, sThreadId, sNameThread);
                }
            });

            $('#delete_thread_id').on('click', function() {
                var sKeyThread = prompt("Write key path id for BBDD");
                if (sKeyThread != null) {
                    startDB(deleteObject, parseInt(sKeyThread));
                }
            });


            // Más manipulación del DOM y eventos...
        } else if (window.location.pathname.match(/\/admin-v2\/moderation/)) {

            (function() {

                document.addEventListener('change', function(event) {
                    if (event.target && event.target.id === 'thread_id_select') {
                        var threadMergeDestination = document.querySelector('input[aria-label="Thread ID"]');
                        if (threadMergeDestination) {
                            // Hacer clic en el input
                            threadMergeDestination.click();

                            // Simular la escritura del valor
                            simulateTyping(threadMergeDestination, event.target.value);
                        }
                    }
                });

                // Función para simular la escritura carácter por carácter
                function simulateTyping(element, value) {
                    var index = -1;

                    function typeChar() {
                        if (index < value.length) {
                            element.value += value[index++];
                            setTimeout(typeChar, 100); // Esperar 100ms antes de escribir el siguiente carácter
                        }
                    }
                    typeChar();
                }

                // Función para manejar los clics en botones específicos
                function handleButtonByTextContent(selector, textContent) {
                    var elements = document.querySelectorAll(selector);
                    elements.forEach(function(element) {
                        if (element.textContent.includes(textContent)) {
                            var button = element.closest('button');
                            if (button) {
                                button.addEventListener('click', buttonClickHandler);
                            }
                        }
                    });
                }

                // Manejador para el clic en el botón
                function buttonClickHandler() {
                    var waitForSubheading = setInterval(function() {
                        var subheadingDiv = document.querySelector('span.subheading');
                        if (subheadingDiv) {
                            clearInterval(waitForSubheading);
                            if (!document.querySelector('span.subheading + div.flex')) {
                                var existingDiv = subheadingDiv.parentNode.querySelector('.flex.pb-4');
                                if (!existingDiv) {
                                    var newDiv = document.createElement('div');
                                    newDiv.className = 'flex pb-4';
                                    newDiv.innerHTML = '<span class="subheading">Find a thread:</span> <div class="v-input d-inline-block ml-4 v-text-field theme--light"><div class="v-input__control"><div class="v-input__slot"><div class="v-text-field__slot"><label aria-hidden="true" class="v-label theme--light" style="left: 0px; right: auto; position: absolute;"></label><select id="thread_id_select" aria-label=""></select></div></div><div class="v-text-field__details"><div class="v-messages theme--light"><div class="v-messages__wrapper"></div></div></div></div></div>';
                                    subheadingDiv.parentNode.appendChild(newDiv);
                                    startDB(loadAll);
                                }
                            }
                        }
                    }, 100);
                }

                // Función para manejar el clic en 'Duplicates'
                function handleDuplicatesClick() {
                    var duplicatesSpans = document.querySelectorAll('span.caption');
                    duplicatesSpans.forEach(function(span) {
                        if (span.textContent.trim() === 'Duplicates') {
                            var duplicatesContainer = span.closest('div'); // Encuentra el div contenedor
                            if (duplicatesContainer) {
                                duplicatesContainer.addEventListener('click', buttonClickHandler);
                            }
                        }
                    });
                }

                // Función para manejar los cambios en la página
                function handlePageChange() {
                    if (window.location.pathname.match(/\/admin-v2\/moderation/)) {
                        handleButtonByTextContent('i.material-icons', 'call_merge');
                        handleDuplicatesClick();
                    }
                }

                // Observador de mutaciones para detectar cambios en el DOM
                var observer = new MutationObserver(function(mutations) {
                    mutations.forEach(function(mutation) {
                        if (mutation.type === 'childList') {
                            handlePageChange();
                        }
                    });
                });

                // Opciones del observador
                var config = {
                    childList: true,
                    subtree: true
                };

                // Inicia la observación
                observer.observe(document.body, config);

                // Ejecuta la función inicialmente
                handlePageChange();
            })();

        }

    };

})();